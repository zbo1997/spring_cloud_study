package com.momo.nacos;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：朱博
 * @description：TODO
 * @date ：2021/2/21 18:31
 */
@RestController
public class HelloController {

    @Value("${name}")
    String name;

    @GetMapping("/hello")
    public String hello() {
        return name;
    }
}
