package com.momo.provider;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pojo.User;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    /**
     * 设置请求合并，可以处理单个请求，也可以处理合并请求
     * @param ids
     * @return
     */
    @GetMapping("/user/{ids}")//假设多个id 传入的参数格式以多个逗号各级「1，2，3。。。」
    public List<User> geyUserByIds(@PathVariable String ids){
        String[] split = ids.split(",");
        System.out.println(ids);
        List<User> list = new ArrayList(10);
        for (String s : split) {
            User user = new User();
            user.setId(Long.parseLong(s));
            user.setUsername("zhubo"+ s);
            user.setPassword("zhubo6984");
            list.add(user);
        }
        return list;
    }


}
