package com.momo.provider;

import com.momo.api.IUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import pojo.User;

import javax.xml.crypto.Data;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Date;

/**
 * 接入对应的IUserService api 公共接口模块
 */
@RestController
public class HelloController implements IUserService {

    @Value("${server.port}")
    Integer port;


    @RequestMapping("/hello")
    @Override
    public String hello(){
        return "Hello Spring Cloud" + port;
    }

    /**
     * 使用请求缓存的
     * @return
     */
    @RequestMapping("/hello2")
    public String hello2(String name){
        System.out.println("请求");
        Date date = new Date();

        return "Hello" + name + date;
    }


    @PostMapping("/user1")
    @Override
    public User addUser(User user){
        return user;
    }

    @PostMapping("/user2")
    public User user2(@RequestBody User user){
        return user;
    }

    @PutMapping("/user3")
    public void updateUser3(User user){
        System.out.println(user.toString());
    }


    @PutMapping("/user4")
    public void updateUser4(@RequestBody User user){
        System.out.println(user.toString());
    }

    @DeleteMapping("/user5")
    public void updateUser5(Integer userId){
        System.out.println(userId);
    }

    @DeleteMapping("/user6/{userId}")
    @Override
    public void deleteUserById(@PathVariable("userId") Integer userId){
        System.out.println(userId);
    }


    /**
     * 因为是 从请求投中获取参数，如果是中文会乱码，所以要使用URLDecoder进行解码
     * @param name
     * @throws UnsupportedEncodingException
     */
    @GetMapping("/user")
    @Override
    public void getUserByName(@RequestHeader String name) throws UnsupportedEncodingException {
        System.out.println(URLDecoder.decode(name,"UTf-8"));
    }
}
