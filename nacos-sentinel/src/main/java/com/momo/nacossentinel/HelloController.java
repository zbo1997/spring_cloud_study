package com.momo.nacossentinel;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ：朱博
 * @description：TODO
 * @date ：2021/2/22 22:17
 */

@RestController
public class HelloController {

    @GetMapping("/hello")
    public String hello(){
        return "helloISentinel";
    }
}
