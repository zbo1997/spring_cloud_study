package com.momo.hystrix.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCollapser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pojo.User;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Future;

@Service
public class UserService {

    @Autowired
    RestTemplate restTemplate;

    /**
     * 这里掉发送给provider中，使用了数组的类型
     * @param ids
     * @return
     */
    @HystrixCommand//如果需要使用注解的方式 那么必须添加@HystrixCommand 这个注解，和继承HystrixCommand 同理
    public List<User> getUserByIds(List<Integer> ids){
        User[] forObject = restTemplate.getForObject("http://my-provider/user/{1}", User[].class, StringUtils.join(ids, ","));
        List<User> users = Arrays.asList(forObject);
        return users;
    }

    /**
     * 使用注解的方式实现请求合并
     * @param id
     * @batchMethod 表示我们需要合并请求的方法
     * @HystrixProperty 表示我们请求合并的一些参数设置
     * @return
     */
    @HystrixCollapser(batchMethod = "getUserByIds",
            collapserProperties = {@HystrixProperty(name = "timerDelayInMilliseconds",value = "200")})
    public Future<User> getUserById(Integer id){
       return null;
    }
}
