package com.momo.hystrix.controller;

import com.momo.hystrix.service.UserService;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import pojo.User;

import java.util.List;

/**
 * 请求合并的HystrixCommand 通过在构造方法注入参和需要调用的接口
 * 来完成一次批处理的"请求合并"
 */
public class UserBatchCommand extends HystrixCommand<List<User>> {

    private List<Integer> ids;

    private UserService userService;

    public UserBatchCommand(List<Integer> ids, UserService userService) {
        /**
         * 这里暂理解设定一些Hystrix的线程隔离策略是根据我们设定的一些command分组名称和command命令来区分的
         */
        super(HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey
                .Factory
                .asKey("batchCmd")
                ).andCommandKey(HystrixCommandKey.Factory.asKey("batchKey")));
        this.ids = ids;
        this.userService = userService;
    }

    @Override
    protected List<User> run() throws Exception {
        return userService.getUserByIds(ids);
    }
}
