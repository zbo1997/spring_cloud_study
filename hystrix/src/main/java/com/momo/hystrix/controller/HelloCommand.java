package com.momo.hystrix.controller;

import com.netflix.hystrix.HystrixCommand;
import org.springframework.web.client.RestTemplate;

/**
 * 通过继承的方法实现服务降级
 */
public class HelloCommand extends HystrixCommand<String> {

    RestTemplate restTemplate;

    /**
     *  因为我们在继承中也要使用缓存机制，但是我们应该确定一个缓存的属性，和需要缓存的key
     *  所以我们只能在
     */
    String name;

    /**
     * 可以同过HystrixCommand的构造方法我们来设置这个发起请求的一些配置
     *
     * @param setter
     */
    protected HelloCommand(Setter setter, RestTemplate restTemplate,String name) {
        super(setter);
        this.restTemplate = restTemplate;
        this.name = name;
    }



    /**
     * 真正发起请求的方法
     * @return
     * @throws Exception
     */
    @Override
    protected String run() throws Exception {
        return restTemplate.getForObject("http://my-provider/hello", String.class);
    }



    /**
     * 请求失败的回掉方法（重写这个方法，Hystrix就可以毁掉进行容错处理）
     * @return 中的返回值和使用@HystrixCommand(fallbackMethod = "error")
     * 中调用的fallbackMethod中制定的方法一样，也是为了区分到底是consumer代码出错
     * 还是调用provider出错的。（打印出具体的错误信息）
     * @return getExecutionException().getMessage() 可以拿到对应的异常信息
     *
     */
    @Override
    protected String getFallback() {
        System.out.println("执行了HelloCommand的getFallback() 方法");
        return "extends-hystrixCommand_getFallback();"+getExecutionException().getMessage();
    }


    /**
     * 通过重写父类的getCacheKey() 方法来返回一个我们指定好需要缓存的key
     * @return
     */
    @Override
    protected String getCacheKey() {
        return this.name;
    }
}
