package com.momo.api;

import org.springframework.web.bind.annotation.*;
import pojo.User;

import java.io.UnsupportedEncodingException;

public interface IUserService {

    @GetMapping("/hello")
    String hello();

    /**
     * 注意这里我们要
     * 一定要用@RequestParam 来修饰这个参数，不然胡找不到这个参数，因为他要去和对应provider中参数名绑定
     * @param name
     * @return
     */
    @GetMapping("/hello2")
    String hello2(@RequestParam("name") String name);

    /**
     * 使用JSON的方式入参
     * @param user
     * @return
     */
    @PostMapping("/user3")
    User addUser(@RequestBody User user);

    /**
     * @PathVariable 使用这个方式入参
     * @param userId
     */
    @DeleteMapping("/user2/{userId}")
    void deleteUserById(@PathVariable("userId") Integer userId);


    /**
     * 使用requestHeader入参，这个必须编码后入参不然会出现乱码
     * 使用URLDecoder.ecode(name,"UTf-8")
     * @param name
     */
    @GetMapping("/user")
    void getUserByName(@RequestHeader("name") String name) throws UnsupportedEncodingException;


}
