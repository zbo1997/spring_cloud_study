package com.momo.nacos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author ：朱博
 * @description：TODO
 * @date ：2021/2/21 18:31
 */
@RestController
public class HelloController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/hellonacos")
    public String hello() {
        return restTemplate.getForObject("http://nacos-provider01/hello", String.class);
    }
}
