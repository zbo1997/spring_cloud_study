package com.momo.opfeigin;

import com.momo.api.IUserService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import pojo.User;

/**
 * @author ：朱博
 * @description :这里直接继承IUserService 就可以实现OpenFeign的功能
 * 使用了可以使得openfeign 一部分呢可以提取出来
 * @date ：2021/2/21 11:40
 */
@FeignClient("my-provider")//服务绑定
public interface HelloService extends IUserService {


}
