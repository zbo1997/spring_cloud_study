package com.momo.opfeigin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pojo.User;

import java.io.UnsupportedEncodingException;

/**
 * @author ：朱博
 * @description：TODO
 * @date ：2021/2/21 11:53
 */
@RestController
public class HelloController {

    @Autowired
    HelloService helloService;

    @GetMapping("/hello")
    public String getHelloService() {
        return helloService.hello();
    }

    @GetMapping("/hello2")
    public String getHelloService(String name) {
        return helloService.hello2(name);
    }

    @PostMapping("/user2")
    public User addUser(User user) {
        return helloService.addUser(user);
    }

    @GetMapping("/user")
    public void getUserByName() throws UnsupportedEncodingException {
        helloService.getUserByName("朱博");
    }
}
